from analysis import text_analysis
import pandas as pd
from pandasql import *

pysqldf = lambda q: sqldf(q, globals())

df = pd.read_csv('gadget.csv')

# Converting linux epoch to tm
df['created_at'] = pd.to_datetime(df['created_at'], unit='s')
df['deadline'] = pd.to_datetime(df['deadline'], unit='s')
df['launched_at'] = pd.to_datetime(df['launched_at'], unit='s')
df['state_changed_at'] = pd.to_datetime(df['state_changed_at'], unit='s')

# calculate the duration of the campaign
df['duration'] = (df['deadline'] - df['launched_at']).dt.days

# calculate the period between the announcement and the launch
df['dev_period'] = (df['launched_at'] - df['created_at']).dt.days

# creating the target
df = pysqldf("""
select *,case
            when pledged >= goal then 1
            else 0
        end as target
FROM df
""")

sentiment = []
classification = []
for i in range(0, df.shape[0]):
    try:
        category = text_analysis.text_classification(df['blurb'][i]).name
        classification.append(category)
    except AttributeError as error:
        classification.append('None')
    except:
        classification.append('Few words')


df['class'] = pd.DataFrame(classification)
# insert sentiment api to the loop above
df['sentiment'] = pd.DataFrame(sentiment)

df.to_csv('dataset_plus_gapi.csv', index=False)