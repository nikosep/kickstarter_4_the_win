from imblearn.over_sampling import SMOTE
from pandasql import *
import seaborn as sns
from sklearn.metrics import f1_score
from sklearn.metrics import explained_variance_score, accuracy_score
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.neural_network import MLPClassifier

from analysis.conf_matrix import cm_analysis

pysqldf = lambda q: sqldf(q, globals())

df = pd.read_csv('dataset_plus_gapi.csv')

df = pysqldf("""select country, currency, fx_rate, goal, static_usd_rate, duration, dev_period, sentiment, class, target
                from df
             """)

df["sentiment"] = df["sentiment"].str.replace("None", "0.0")
df['sentiment'] = pd.to_numeric(df['sentiment'])

df["class"] = df["class"].str.split("&", n=1, expand=True)
# df["class"] = df["class"].str.split("/", n=2)


# Plot a correlation matrix among all the variables
sns.pairplot(df)
plt.show()

# Get dummies
df = pd.get_dummies(df)

# Sampling the data
df = df.sample(frac=1)
df = df.dropna()
# We store the target into y list
y = df['target']
del df['target']
df = df.dropna()

# SMOTE technique for Oversampling
sm = SMOTE(random_state=42)
X_res, y_res = sm.fit_resample(df, y)
cols = df.columns
X_res = pd.DataFrame(X_res, columns=cols)
y_res = pd.DataFrame(y_res)
# end SMOTE

# splitting the data into training and testing set
X_train, X_test, y_train, y_test = train_test_split(X_res, y_res, test_size=0.33, random_state=0)

# Using Random Forest classifier
clf = RandomForestClassifier(n_estimators=2000, max_depth=15, random_state=1).fit(X_train, y_train)

# Using Cross-Validation with 5 folds to see the initial accuracy
scores = cross_val_score(clf, X_res, y_res, cv=5)
print(scores)

# =============== TRAINING SET ===============
# Finding the training error
predictions_training = clf.predict(X_train)
# Print the training accuracy
f1_score(y_train, predictions_training, average='binary')

# =============== TESTING SET ===============
# Finding the testing error
predictions_testing = clf.predict(X_test)
# Print the testing accuracy
f1_score(y_test, predictions_testing, average='binary')
# accuracy_score(y_test, predictions_testing)
# confusion matrix for deeper understating of the accuracy
cm_analysis(y_test, predictions_testing, clf.classes_, ymap=None, figsize=(10, 10))

# Plot the feature importance
feat_labels = X_train.columns
feature_importance = dict(zip(feat_labels, clf.feature_importances_))
plt.bar(range(len(feature_importance)), list(feature_importance.values()), align='center')
plt.xticks(range(len(feature_importance)), list(feature_importance.keys()))
plt.show()

import xgboost as xgb
# Print the training accuracy
ss = xgb.XGBClassifier(n_estimators=1000, max_depth=2, min_child_weight=0.5, random_state=7).fit(X_train, y_train)
predictions_training = ss.predict(X_train)
# Print the training accuracy
f1_score(y_train, predictions_training, average='binary')

predictions_testing = ss.predict(X_test)
# Print the testing accuracy
f1_score(y_test, predictions_testing, average='binary')
