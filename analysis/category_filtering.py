from pandasql import *
import pandas as pd
import glob
import io
import json
import warnings
# df = pd.read_csv(r'C:\Users\ultimate\Documents\Kickstarter028.csv', sep=',')
pysqldf = lambda q: sqldf(q, globals())
warnings.simplefilter(action = "ignore", category = RuntimeWarning)

# Import all the csv
path = r'C:\Users\Nikos Epitropakis\Downloads\kickstarter_data'
all_files = glob.glob(path + "/*.csv")

li = []
for filename in all_files:
    df = pd.read_csv(filename, index_col=None,sep=',')
    li.append(df)

frame = pd.concat(li, axis=0, ignore_index=True)

data = pd.io.json.json_normalize(frame.category.apply(json.loads))

dataset = pd.concat([frame, data], axis=1)

# filtering the columns
exp = pysqldf("""
select blurb, slug, converted_pledged_amount, country, created_at, currency, deadline, fx_rate, goal, is_backing, launched_at,
       name, pledged, slug, state_changed_at, static_usd_rate, usd_pledged, usd_type
from dataset
""")

# 1256/2918 = 43%
gadgets = pysqldf("""select * from exp where slug='technology/gadgets'""")
gadgets.to_csv('gadget.csv', index=False)
