import os
import six
from google.cloud import language
from google.cloud import language_v1
from google.cloud.language import enums
from google.cloud.language import types

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "IntelliGeo-625bcf1f856f.json"


def sample_analyze_sentiment(content):
    client = language_v1.LanguageServiceClient()

    # content = 'Your text to analyze, e.g. Hello, world!'

    if isinstance(content, six.binary_type):
        content = content.decode('utf-8')

    type_ = enums.Document.Type.PLAIN_TEXT
    document = {'type': type_, 'content': content}

    response = client.analyze_sentiment(document)
    sentiment = response.document_sentiment
    # print('Score: {}'.format(sentiment.score))
    # print('Magnitude: {}'.format(sentiment.magnitude))
    return sentiment.score, sentiment.magnitude

# sample_analyze_sentiment("Your text to analyze, e.g. Hello, world!")

def text_classification(text):
    text = text
    client = language.LanguageServiceClient()

    if isinstance(text, six.binary_type):
        text = text.decode('utf-8')

    document = types.Document(
        content=text.encode('utf-8'),
        type=enums.Document.Type.PLAIN_TEXT)

    categories = client.classify_text(document).categories

    for category in categories:
        # print(u'=' * 20)
        # print(u'{:<16}: {}'.format('name', category.name))
        # print(u'{:<16}: {}'.format('confidence', category.confidence))
        return category